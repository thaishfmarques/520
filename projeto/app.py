import modulos.banco as banco
import threading


if __name__ == '__main__':
    user = input('Nickname: ')
    try:
        f = threading.Thread(target=banco.ler_msg)
        f.start()
    except Exception as e:
        print('Falta ao criar thread: {}'.format(e))
    
    while f.isAlive:
        msg = input()
        banco.cadastrar(user, msg)