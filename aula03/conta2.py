from conta import Conta, Poupanca
# import conta

c1 = Conta('daniel', 123456, 1500)
c2 = Poupanca('maria', 654321, 1500)

c2.transferir(500, c1)

print(c1.saldo, c2.saldo, sep="\n")
print(c1, c2, sep='\n')
